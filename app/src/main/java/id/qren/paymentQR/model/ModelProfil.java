package id.qren.paymentQR.model;

public class ModelProfil {

    private int id;
    private String merchantApiKey;
    private String accessToken;
    private String merchantName;
    private String merchantOutletName;
    private String merchantEmail;
    private String merchantPhone;
    private String merchantAddress;
    private String merchantBalance;
    private String tmoneyUsername;
    private String tmoneyId;
    private String fusionId;
    private String tmoneyToken;
    private String tmoneyBalance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMerchantApiKey() {
        return merchantApiKey;
    }

    public void setMerchantApiKey(String merchantApiKey) {
        this.merchantApiKey = merchantApiKey;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantOutletName() {
        return merchantOutletName;
    }

    public void setMerchantOutletName(String merchantOutletName) {
        this.merchantOutletName = merchantOutletName;
    }

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getMerchantPhone() {
        return merchantPhone;
    }

    public void setMerchantPhone(String merchantPhone) {
        this.merchantPhone = merchantPhone;
    }

    public String getMerchantAddress() {
        return merchantAddress;
    }

    public void setMerchantAddress(String merchantAddress) {
        this.merchantAddress = merchantAddress;
    }

    public String getMerchantBalance() {
        return merchantBalance;
    }

    public void setMerchantBalance(String merchantBalance) {
        this.merchantBalance = merchantBalance;
    }

    public String getTmoneyUsername() {
        return tmoneyUsername;
    }

    public void setTmoneyUsername(String tmoneyUsername) {
        this.tmoneyUsername = tmoneyUsername;
    }

    public String getTmoneyId() {
        return tmoneyId;
    }

    public void setTmoneyId(String tmoneyId) {
        this.tmoneyId = tmoneyId;
    }

    public String getFusionId() {
        return fusionId;
    }

    public void setFusionId(String fusionId) {
        this.fusionId = fusionId;
    }

    public String getTmoneyToken() {
        return tmoneyToken;
    }

    public void setTmoneyToken(String tmoneyToken) {
        this.tmoneyToken = tmoneyToken;
    }

    public String getTmoneyBalance() {
        return tmoneyBalance;
    }

    public void setTmoneyBalance(String tmoneyBalance) {
        this.tmoneyBalance = tmoneyBalance;
    }
}
