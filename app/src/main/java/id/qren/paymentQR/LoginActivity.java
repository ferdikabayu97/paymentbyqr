package id.qren.paymentQR;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import id.qren.paymentQR.R;
import id.qren.paymentQR.model.ModelProfil;
import id.qren.paymentQR.datasource.ProfileDatasource;
import id.qren.qrenloginlib.login.LoginIdentifier;
import id.qren.qrenloginlib.login.LoginInteractor;
import id.qren.qrenloginlib.model.ResponseWrapper;

public class LoginActivity extends AppCompatActivity implements LoginInteractor.LoginRequestListener {

    private LoginInteractor interactor = new LoginInteractor();

    private EditText userName;
    private EditText pin;
    private Button buttonLogin;

    ProfileDatasource profileDatasource = new ProfileDatasource(this);

    String accessToken, merchantName, outletName, merchantEmail, merchantPhone, merchantAddress, merchantApiKey, merchantBalance;
    String tmoneyUserName, tmoneyId, tmoneyFusion, tmoneyToken, tmoneyBalance, resultCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName = findViewById(R.id.loginActivity_userName_editText);
        pin = findViewById(R.id.loginActivity_Pin_editText);
        buttonLogin = findViewById(R.id.loginActivity_login_button);


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin(userName.getText().toString(), pin.getText().toString());

            }
        });
    }

    private void doLogin(String loginUsername, String loginPin) {
        String imei = "";
        String OSPLayerId = "";
        String deviceId = "";
        String terminal = "MERCHANT-API";
        interactor.getMerchantProfile(this, new LoginIdentifier(loginUsername, imei, loginPin, OSPLayerId, deviceId, terminal));
    }


    @Override
    public void onLoginSuccess(ResponseWrapper response) {
        resultCode = response.getResultCode();
        if (resultCode.equals("0")) {
            accessToken = response.getAccessToken();
            merchantName = response.getMerchantName();
            outletName = response.getMerchantOutletName();
            merchantEmail = response.getMerchantEmail();
            merchantPhone = response.getMerchantPhone();
            merchantAddress = response.getMerchantAddress();
            merchantApiKey = response.getMerchantApiKey();
            merchantBalance = response.getMerchantBalance();
            tmoneyUserName = response.getTmoneyUsername();
            tmoneyId = response.getTmoneyId();
            tmoneyFusion = response.getFusionId();
            tmoneyToken = response.getTmoneyToken();
            tmoneyBalance = response.getTmoneyBalance();

            System.out.println("Alamat Merchant: " + merchantAddress);

            ModelProfil modelProfil = new ModelProfil();
            modelProfil.setId(0);
            modelProfil.setMerchantApiKey(merchantApiKey);
            modelProfil.setAccessToken(accessToken);
            modelProfil.setMerchantName(merchantName);
            modelProfil.setMerchantOutletName(outletName);
            modelProfil.setMerchantEmail(merchantEmail);
            modelProfil.setMerchantPhone(merchantPhone);
            modelProfil.setMerchantAddress(merchantAddress);
            modelProfil.setMerchantBalance(merchantBalance);
            modelProfil.setTmoneyUsername(tmoneyUserName);
            modelProfil.setTmoneyId(tmoneyId);
            modelProfil.setFusionId(tmoneyFusion);
            modelProfil.setTmoneyToken(tmoneyToken);
            modelProfil.setTmoneyBalance(tmoneyBalance);

            profileDatasource.deleteProfil();
            profileDatasource.insert(modelProfil);

            Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
            mainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mainActivity);
            finish();
        } else {
            Toast.makeText(LoginActivity.this, "Login Gagal, Periksa kembali nomor HP atau PIN anda", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoginFailure(String s) {

    }
}
