package id.qren.paymentQR;

import org.json.JSONException;
import org.json.JSONObject;

public class QRPayment_Model {
    private String InvoiceId,status,message,trxId,amount;

    public static QRPayment_Model fromJson(JSONObject objek){

        try{
            QRPayment_Model data = new QRPayment_Model();

            data.InvoiceId = objek.getString("invoice");
            data.status = objek.getString("status");
            data.message = objek.getString("message");
            data.trxId = objek.getString("trxId");
            data.amount = objek.getString("amount");

            return data;
        }catch(JSONException e){
            return null;
        }
    }


    public String getInvoiceId() {
        return InvoiceId;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getTrxId() {
        return trxId;
    }

    public String getAmount() {
        return amount;
    }
}
