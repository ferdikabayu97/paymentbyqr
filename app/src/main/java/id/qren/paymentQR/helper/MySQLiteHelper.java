package id.qren.paymentQR.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_MERCHANT = "merchant";
    public static final String MERCHANT_ID = "id";
    public static final String MERCHANT_API_KEY = "api_key";
    public static final String MERCHANT_ACCESS_TOKEN = "access_token";
    public static final String MERCHANT_NAME = "merchant_name";
    public static final String MERCHANT_OUTLET_NAME = "merchant_outlet_name";
    public static final String MERCHANT_EMAIL = "merchant_email";
    public static final String MERCHANT_PHONE = "merchant_phone";
    public static final String MERCHANT_ADDRESS = "merchant_address";
    public static final String MERCHANT_BALANCE = "merchant_balance";
    public static final String TMONEY_USERNAME = "tmoney_username";
    public static final String TMONEY_ID = "tmoney_id";
    public static final String TMONEY_FUSION_ID = "fusion_id";
    public static final String TMONEY_TOKEN = "tmoney_token";
    public static final String TMONEY_BALANCE = "tmoney_balance";

    private static final String Database_Name = "parking.db";
    private static final int Database_Version = 1;

    private static final String CREATE_TABLE_MERCHANT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_MERCHANT + "(" +
            MERCHANT_ID + " text," +
            MERCHANT_API_KEY + " text," +
            MERCHANT_ACCESS_TOKEN + " text," +
            MERCHANT_NAME + " text," +
            MERCHANT_OUTLET_NAME + " text," +
            MERCHANT_EMAIL + " text," +
            MERCHANT_PHONE + " text," +
            MERCHANT_ADDRESS + " text," +
            MERCHANT_BALANCE + " text," +
            TMONEY_USERNAME + " text," +
            TMONEY_ID + " text," +
            TMONEY_FUSION_ID + " text," +
            TMONEY_TOKEN + " text," +
            TMONEY_BALANCE + " text);";

    public MySQLiteHelper(Context context) {
        super(context, Database_Name, null, Database_Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MERCHANT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MERCHANT);
        onCreate(db);
    }
}
