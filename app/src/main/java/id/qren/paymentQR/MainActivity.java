package id.qren.paymentQR;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.zxing.Result;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.qren.paymentQR.R;
import id.qren.paymentQR.datasource.ProfileDatasource;
import id.qren.paymentQR.model.ModelProfil;
import id.qren.paymentQR.tools.Constants;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;
    private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    ProgressDialog mDialog;


    TextView namaLoket, namaMerchant, tagLayout;
    LinearLayout layoutEntry, layoutExit, layoutInputPlat, layoutScanner;
    EditText inputPlatNomor;
    Button buttonConfirm;

    String accessToken, merchantName, outletName, merchantEmail, merchantPhone, merchantAddress, merchantApiKey, merchantBalance;
    String tmoneyUserName, tmoneyId, tmoneyFusion, tmoneyToken, tmoneyBalance, resultCode;

    String platNomor;
    ProfileDatasource profilDb;
    ModelProfil modelProfil = new ModelProfil();

    protected void dialog(String a){
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(a);
        mDialog.setCancelable(false);
        mDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);



        namaLoket = findViewById(R.id.labelNamaLoket);
        namaMerchant = findViewById(R.id.labelNamaMerchant);
        layoutEntry = findViewById(R.id.layoutEntry);
        layoutExit = findViewById(R.id.layoutExit);
        layoutInputPlat = findViewById(R.id.layoutInputPlat);
        inputPlatNomor = findViewById(R.id.inputPlatNomor);
        buttonConfirm = findViewById(R.id.buttonConfirm);

        /* digunakan bila menggunakan scanner untuk parkir keluar (uncomment)*/
        layoutScanner = findViewById(R.id.layoutScanner);

        scannerView = findViewById(R.id.scan_layout);

        int currentApiVersion = Build.VERSION.SDK_INT;

        if(currentApiVersion >=  Build.VERSION_CODES.M)
        {
            if(checkPermission())
            {
//                Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
            }
            else
            {
                requestPermission();
            }
        }
        layoutScanner.setVisibility(View.GONE);

        profilDb = new ProfileDatasource(this);
        modelProfil = profilDb.getMerchantProfil();

        merchantName = modelProfil.getMerchantName();
        outletName =  modelProfil.getMerchantOutletName();

        System.out.println("Nama Merchant :" + merchantName);
        System.out.println("Nama Outlet Merchant :" + outletName);

        layoutInputPlat.setVisibility(View.GONE);
        namaLoket.setText(outletName);
        namaMerchant.setText(merchantName);

        layoutEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutEntry.setBackgroundResource(R.drawable.button_rounded_green);
                layoutExit.setBackgroundResource(R.drawable.button_rounded_yellow);
                layoutInputPlat.setVisibility(View.VISIBLE);
//                tagLayout.setText("tagMasuk");
                layoutScanner.setVisibility(View.GONE);
                inputPlatNomor.requestFocus();
                scannerView.setFlash(false);
            }
        });

        layoutExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutExit.setBackgroundResource(R.drawable.button_rounded_green);
                layoutEntry.setBackgroundResource(R.drawable.button_rounded_yellow);
//                layoutInputPlat.setVisibility(View.VISIBLE);
                layoutScanner.setVisibility(View.VISIBLE);
                layoutInputPlat.setVisibility(View.GONE);
//                tagLayout.setText("tagKeluar");
                inputPlatNomor.requestFocus();
//                buttonConfirm.setText("BAYAR");
                scannerView.setFlash(true);

            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
//                String tag = tagLayout.getText().toString();
//                if (tag.equals("tagMasuk")) {
                    platNomor = inputPlatNomor.getText().toString();
                    dialog("Menunggu Parkir Masuk");
                if (platNomor.matches("[0-9]+")) {
                    getInvoice(platNomor);
                } else {
                    Toast.makeText(MainActivity.this, "Periksa kembali Plat Nomor yang diinput", Toast.LENGTH_LONG).show();
                    mDialog.dismiss();
                    onResume();
                }
//                } else {
//                    dialog("Menunggu Parkir Keluar");
//
//                    platNomor = inputPlatNomor.getText().toString();
//                    if (platNomor.length() < 5) {
//                        Toast.makeText(MainActivity.this, "Periksa kembali Plat Nomor yang diinput", Toast.LENGTH_LONG).show();
//                    } else {
//                        getInvoice(platNomor);
//                    }
//                }

            }
        });


        Button b = (Button) findViewById(R.id.logout);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert
                        .setTitle("Mengeluarkan Merchant dari Device ?")
                        .setIcon(R.mipmap.ic_launcher_round)
                        .setMessage("Jika keluar dari Device, anda wajib melakukan login Merchant kembali")
                        .setNegativeButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Intent backHome = new Intent(MainActivity.this, LoginActivity.class);
                                profilDb.deleteProfil();
                                startActivity(backHome);
                                finish();
                            }
                        }).setPositiveButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.create();
                alert.show();
            }
        });

    }

    private boolean checkPermission()
    {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission()
    {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    @Override
    public void onResume() {
        super.onResume();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if(scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);

                scannerView.startCamera();
//                scannerView.setFlash(true);

            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
        scannerView.setFlash(false);

    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
        scannerView.setFlash(false);

    }

    @Override
    public void handleResult(Result result) {
        final String myResult = result.getText();
//        Toast.makeText(getApplicationContext(), result.getText(), Toast.LENGTH_LONG).show();
        dialog("Menunggu Parkir Keluar");

                    platNomor = result.getText();
                    if (platNomor.matches("[0-9]+")) {
                        getInvoice(platNomor);
                    } else {
                        Toast.makeText(MainActivity.this, "Periksa kembali Plat Nomor yang diinput", Toast.LENGTH_LONG).show();
                        mDialog.dismiss();
                        onResume();
                    }

    }



    private void getInvoice(final String nominal) {

        ProfileDatasource profilDb;
        ModelProfil modelProfil;

        profilDb = new ProfileDatasource(this);
        modelProfil = profilDb.getMerchantProfil();



        Map<String, Object> bodyParams = new HashMap<>();

        bodyParams.put("merchantApiKey", modelProfil.getMerchantApiKey());
        bodyParams.put("nominal", nominal);
        bodyParams.put("staticQR", "0");
        bodyParams.put("invoiceName", modelProfil.getMerchantApiKey());
        bodyParams.put("qrGaruda", "1");
        bodyParams.put("info","Testing");

        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", Constants.basicAuth);
        headers.put("Content-Type", Constants.contentType);

        AndroidNetworking.post(Constants.baseUrl+Constants.epCreateInvoice)
                .setPriority(Priority.HIGH)
                .addHeaders(headers)
                .addBodyParameter(bodyParams)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());
                        try {
                            Log.d("test",""+response);
                            if (response.getString("resultCode").equals("0")) {
                                String invoiceId = response.getString("invoiceId");
                                String content = response.getString("content");
                                String timeStamp = response.getString("timeStamp");

                                String[] output = content.split("5802ID59");
                                int leng = Integer.parseInt(output[1].substring(0,2));

                                Log.d("test",leng+"");
                                String namamerchant = output[1].substring(2,leng+2);
                                mDialog.dismiss();
                                Intent intentShowQR = new Intent(MainActivity.this, SuccessExitActivity.class);
                                intentShowQR.putExtra("qrContent", content);
                                intentShowQR.putExtra("invoiceId", invoiceId);
                                intentShowQR.putExtra("NamaMerchant", namamerchant);
                                intentShowQR.putExtra("timeStamp", timeStamp);
                                intentShowQR.putExtra("amount", nominal);
                                intentShowQR.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intentShowQR);

                            } else {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });

    }




}
