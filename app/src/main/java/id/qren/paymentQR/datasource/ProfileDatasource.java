package id.qren.paymentQR.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import id.qren.paymentQR.helper.MySQLiteHelper;
import id.qren.paymentQR.model.ModelProfil;

public class ProfileDatasource {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumn = {
            MySQLiteHelper.MERCHANT_ID, MySQLiteHelper.MERCHANT_API_KEY, MySQLiteHelper.MERCHANT_ACCESS_TOKEN, MySQLiteHelper.MERCHANT_NAME, MySQLiteHelper.MERCHANT_OUTLET_NAME, MySQLiteHelper.MERCHANT_EMAIL,
            MySQLiteHelper.MERCHANT_PHONE, MySQLiteHelper.MERCHANT_ADDRESS, MySQLiteHelper.MERCHANT_BALANCE, MySQLiteHelper.TMONEY_USERNAME,
            MySQLiteHelper.TMONEY_ID, MySQLiteHelper.TMONEY_FUSION_ID, MySQLiteHelper.TMONEY_TOKEN, MySQLiteHelper.TMONEY_BALANCE
    };

    public boolean CheckIsDataAlreadyInDBorNot() {
        String Query = "Select * from " + MySQLiteHelper.TABLE_MERCHANT + " where " + MySQLiteHelper.MERCHANT_ID + " = 0";
        Cursor cursor = database.rawQuery(Query,null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public ProfileDatasource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    private void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    private void close() {
        dbHelper.close();
    }

    public void insert(ModelProfil profilMerchant) {
        open();
        if (!CheckIsDataAlreadyInDBorNot()) {
            ContentValues values = new ContentValues();
            values.put(MySQLiteHelper.MERCHANT_ID, profilMerchant.getId());
            values.put(MySQLiteHelper.MERCHANT_API_KEY, profilMerchant.getMerchantApiKey());
            values.put(MySQLiteHelper.MERCHANT_ACCESS_TOKEN, profilMerchant.getAccessToken());
            values.put(MySQLiteHelper.MERCHANT_NAME, profilMerchant.getMerchantName());
            values.put(MySQLiteHelper.MERCHANT_OUTLET_NAME, profilMerchant.getMerchantOutletName());
            values.put(MySQLiteHelper.MERCHANT_EMAIL, profilMerchant.getMerchantEmail());
            values.put(MySQLiteHelper.MERCHANT_PHONE, profilMerchant.getMerchantPhone());
            values.put(MySQLiteHelper.MERCHANT_ADDRESS, profilMerchant.getMerchantAddress());
            values.put(MySQLiteHelper.MERCHANT_BALANCE, profilMerchant.getMerchantBalance());
            values.put(MySQLiteHelper.TMONEY_USERNAME, profilMerchant.getTmoneyUsername());
            values.put(MySQLiteHelper.TMONEY_ID, profilMerchant.getTmoneyId());
            values.put(MySQLiteHelper.TMONEY_FUSION_ID, profilMerchant.getFusionId());
            values.put(MySQLiteHelper.TMONEY_TOKEN, profilMerchant.getTmoneyToken());
            values.put(MySQLiteHelper.TMONEY_BALANCE, profilMerchant.getTmoneyBalance());
            database.insert(MySQLiteHelper.TABLE_MERCHANT, null, values);
        }
        close();
    }

    public ModelProfil getMerchantProfil() {
        open();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_MERCHANT, //nama tabel
                allColumn, // nama kolom
                MySQLiteHelper.MERCHANT_ID+" = ?", //where
                new String[]{String.valueOf("0")}, // value
                null, //group by
                null, // having
                null,//order
                null); // limit

        ModelProfil profil = new ModelProfil();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                profil.setId(cursor.getInt(0));
                profil.setMerchantApiKey(cursor.getString(1));
                profil.setAccessToken(cursor.getString(2));
                profil.setMerchantName(cursor.getString(3));
                profil.setMerchantOutletName(cursor.getString(4));
                profil.setMerchantEmail(cursor.getString(5));
                profil.setMerchantPhone(cursor.getString(6));
                profil.setMerchantAddress(cursor.getString(7));
                profil.setMerchantBalance(cursor.getString(8));
                profil.setTmoneyUsername(cursor.getString(9));
                profil.setTmoneyId(cursor.getString(10));
                profil.setFusionId(cursor.getString(11));
                profil.setTmoneyToken(cursor.getString(12));
                profil.setTmoneyBalance(cursor.getString(13));
            }
            cursor.close();
        }
        close();
        return profil;
    }

    public void deleteProfil(){
        open();
        database.delete(MySQLiteHelper.TABLE_MERCHANT, MySQLiteHelper.MERCHANT_ID + " = ?", new String[] { "0" });
        close();
    }
}
