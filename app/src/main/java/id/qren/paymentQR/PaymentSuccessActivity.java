package id.qren.paymentQR;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import id.qren.paymentQR.R;

import id.qren.paymentQR.datasource.ProfileDatasource;
import id.qren.paymentQR.model.ModelProfil;
import id.qren.paymentQR.tools.Constants;
import id.qren.paymentQR.utils.SunmiPrintHelper;



public class PaymentSuccessActivity extends AppCompatActivity {

    ProgressDialog mDialog;


    TextView txtNamaMerchant,txtTimeStamp,txtInvoice, txtAmmount;
    Button ok;
    String invoice,amount,transactionId,namaMerchant,timeStamp;

    public String jadiuang(int a){
        Double ad = new Double(a);
        Locale currentLocale = new Locale("id", "ID");
        Currency currentCurrency = Currency.getInstance(currentLocale);
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);
        String val = currencyFormatter.format(ad);
        return val;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);
        mDialog = new ProgressDialog(PaymentSuccessActivity.this);
        mDialog.setMessage("Memproses Pembayaran");
        mDialog.setCancelable(false);
        mDialog.show();
        timeStamp = getIntent().getStringExtra("timeStamp");
        invoice = getIntent().getStringExtra("Invoice");
        amount = getIntent().getStringExtra("Amount");
        transactionId = getIntent().getStringExtra("TransactionId");
        namaMerchant = getIntent().getStringExtra("namaMerchant");

        initPrinter();
//          erase sumni
        printInvoice( amount,  invoice, namaMerchant, timeStamp);

        txtInvoice = findViewById(R.id.invoice);
        txtNamaMerchant = findViewById(R.id.namaMerchant);
        txtTimeStamp = findViewById(R.id.timeStamp);
        txtAmmount = findViewById(R.id.sumAmmount);
        ok = findViewById(R.id.okPayment);

        txtInvoice.setText(invoice);
        txtNamaMerchant.setText(namaMerchant);
        txtTimeStamp.setText(timeStamp);
        txtAmmount.setText(jadiuang(Integer.parseInt(amount)));
        mDialog.dismiss();

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(PaymentSuccessActivity.this, MainActivity.class);

                startActivity(i);
            }
        });
    }

    private void initPrinter() {
        SunmiPrintHelper.getInstance().initSunmiPrinterService(this);
    }

    private void printInvoice( String amount,  String invoice, String namaMerchant, String timeStamp) {
        SunmiPrintHelper cetak = SunmiPrintHelper.getInstance();

        ProfileDatasource profilDb;
        ModelProfil modelProfil;

        profilDb = new ProfileDatasource(this);
        modelProfil = profilDb.getMerchantProfil();

        String[] tanggalWaktuTransaksi = timeStamp.split(" ");
        String tanggalTransaksi = tanggalWaktuTransaksi[0];
        String jamTransaksi = tanggalWaktuTransaksi[1];


        String[] textInvoice = new String[]{"Invoice", ":", invoice};
        String[] textHarga = new String[]{"Biaya", ":", jadiuang(Integer.parseInt(amount))};
        String[] textNamaMerchant = new String[]{"Merchant", ":", namaMerchant};
        String[] textTanggalTransaksi = new String[]{"Waktu Trx", ":", tanggalTransaksi};
        String[] textJamTransaksi = new String[]{"", "", jamTransaksi};

        /* set align for table */
        int[] width = new int[]{2,1,2};
        int[] align = new int[]{0,0,2};

        cetak.setAlign(1);
        cetak.printText(modelProfil.getMerchantName().toUpperCase(), 32, true, false );
        cetak.print1Line();
        cetak.printText("Bukti Bayar QR Payment", 28, true, true);
        cetak.print2Line();
        cetak.printQr(invoice, 8, 30);
        cetak.print1Line();
        cetak.printText("Invoice", 20, false, false);
        cetak.print1Line();
        cetak.printText(invoice, 32, true, false);
        cetak.print1Line();
        cetak.printText("------------------------------------------------------", 14, false, false);
        cetak.printTable(textInvoice, width, align);
        cetak.printTable(textHarga, width, align);
        cetak.printTable(textNamaMerchant, width, align);
        cetak.printTable(textTanggalTransaksi, width, align);
        cetak.printTable(textJamTransaksi, width, align);
        cetak.printText("------------------------------------------------------", 14, false, false);
        cetak.print2Line();
        cetak.printText("-- TERIMA KASIH --", 24, false, false);
        cetak.print2Line();
        cetak.feedPaper();
    }
}
