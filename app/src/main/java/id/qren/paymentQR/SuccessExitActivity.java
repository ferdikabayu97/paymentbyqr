package id.qren.paymentQR;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.zxing.WriterException;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import id.qren.paymentQR.R;

import id.qren.paymentQR.datasource.ProfileDatasource;
import id.qren.paymentQR.model.ModelProfil;
import id.qren.paymentQR.tools.Constants;
import id.qren.paymentQR.utils.SunmiPrintHelper;

public class SuccessExitActivity extends AppCompatActivity {
    ImageView imgQR;
    TextView txtNamaMerchant, txtTimeStamp, txtAmmount;
    String qrContent,amount,invoiceId,TrxId,namaMerchant,timeStamp;
    Bitmap bitmap;
    String TAG = "GenerateQRCode";
    ProgressDialog mDialog;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://middleware-qren-middleware-dev.vsan-apps.playcourt.id/");
        } catch (URISyntaxException e) {}
    }

    private void initPrinter() {
        SunmiPrintHelper.getInstance().initSunmiPrinterService(this);
    }


    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    public String jadiuang(int a){
        Double ad = new Double(a);
        Locale currentLocale = new Locale("id", "ID");
        Currency currentCurrency = Currency.getInstance(currentLocale);
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);
        String val = currencyFormatter.format(ad);
        return val;
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            SuccessExitActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.d("test",""+data);
                    initPrinter();
                    final QRPayment_Model status = QRPayment_Model.fromJson(data);
                    if (status.getStatus().equals("0")){
                        ProfileDatasource profilDb;
                        ModelProfil modelProfil;

                        profilDb = new ProfileDatasource(SuccessExitActivity.this);
                        modelProfil = profilDb.getMerchantProfil();
                        TrxId = status.getTrxId();

                        mDialog = new ProgressDialog(SuccessExitActivity.this);
                        mDialog.setMessage("Memproses Pembayaran");
                        mDialog.setCancelable(false);
                        mDialog.show();

                        Intent go = new Intent(SuccessExitActivity.this,PaymentSuccessActivity.class);
                        go.putExtra("Invoice",invoiceId);
                        go.putExtra("Amount",amount);
                        go.putExtra("namaMerchant",namaMerchant);
                        go.putExtra("timeStamp",timeStamp);
                        go.putExtra("TransactionId",status.getTrxId());
                        startActivity(go);
                        finish();

                    }else{
                        Log.d("test",status.getStatus());

                        mDialog.setMessage(status.getMessage());
                        mDialog.setCancelable(true);
                        mDialog.show();
                        mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                Intent kembali = new Intent(SuccessExitActivity.this,MainActivity.class);
                                startActivity(kembali);
                                finish();
                            }
                        });

                    }
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_exit);

        mSocket.connect();

        qrContent = getIntent().getStringExtra("qrContent");
        amount = getIntent().getStringExtra("amount");
        invoiceId = getIntent().getStringExtra("invoiceId");
        timeStamp = getIntent().getStringExtra("timeStamp");
        namaMerchant = getIntent().getStringExtra("NamaMerchant");

        txtTimeStamp = findViewById(R.id.timeStamp);
        txtNamaMerchant= findViewById(R.id.namaMerchant);
        imgQR = (ImageView) findViewById(R.id.qrPayment);
        txtAmmount = findViewById(R.id.sumAmmount);

        txtTimeStamp.setText(timeStamp);
        txtNamaMerchant.setText(namaMerchant);
        txtAmmount.setText(jadiuang(Integer.parseInt(amount)));


        QRGEncoder qrgEncoder = new QRGEncoder(qrContent, null, QRGContents.Type.TEXT,700);



        try {
            // Getting QR-Code as Bitmap
            bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            imgQR.setImageBitmap(bitmap);
            mSocket.on(invoiceId, onNewMessage);


        } catch (WriterException e) {
            Log.v(TAG, e.toString());
        }



    }
}
