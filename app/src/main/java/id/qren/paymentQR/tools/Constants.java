package id.qren.paymentQR.tools;

public class Constants {


    /* Constant for API */
    public static String basicAuth = "Basic dG1vbmV5OmZmODY2ZjViNjE1NGJiYjdkOTc4ZTUyNDNiNDkzMjBiMGQxYWQ2N2M=";
    public static String contentType = "application/x-www-form-urlencoded";
    public static String baseUrl = "https://qren-api.tmoney.co.id/paybyqr";
    public static String epCreateInvoice = "/createinvoice/";

}
