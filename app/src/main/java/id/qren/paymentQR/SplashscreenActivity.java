package id.qren.paymentQR;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import id.qren.paymentQR.R;

import id.qren.paymentQR.datasource.ProfileDatasource;
import id.qren.paymentQR.model.ModelProfil;

public class SplashscreenActivity extends AppCompatActivity {
    ImageView imageLogo;

    TextView textTagline;
    ModelProfil modelUser = new ModelProfil();
    ProfileDatasource userDatasource;
    String userId;


    Runnable stopSplashRunnable = new Runnable() {
        @Override
        public void run() {
            if (userId != null) {
                Intent goMainActivity = new Intent(SplashscreenActivity.this, MainActivity.class);
                startActivity(goMainActivity);
                finish();
            } else {
                Intent goMainActivity = new Intent(SplashscreenActivity.this, LoginActivity.class);
                startActivity(goMainActivity);
                finish();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        userDatasource = new ProfileDatasource(this);
        modelUser = userDatasource.getMerchantProfil();
        userId = modelUser.getMerchantApiKey();

        System.out.println("User ID : " + userId);
        imageLogo = (ImageView) findViewById(R.id.logoSplash);


        new Handler().postDelayed(this.stopSplashRunnable, 1300L);
        try {
            userDatasource = new ProfileDatasource(this);
            modelUser = userDatasource.getMerchantProfil();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Hawk.init(getApplicationContext()).build();
    }
}

